# Simple Noise Algorithm Map Generator

Built to show off a few noise algorithms for a work book club. WASM version hosted online here: https://thatzopoulos.gitlab.io/noise-maps/

I use the bracket-lib crate for most of the functionality.