use bracket_color::prelude::*;
use bracket_noise::prelude::*;
use bracket_random::prelude::*;

use bracket_terminal::prelude::*;
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

#[cfg(target_arch = "wasm32")]
#[cfg_attr(target_arch = "wasm32", wasm_bindgen(start))]
pub fn wasm_main() {
    main();
}

pub const MAP_WIDTH: usize = 160;
pub const MAP_HEIGHT: usize = 100;
pub const MAP_TILES: usize = MAP_WIDTH * MAP_HEIGHT;

#[derive(PartialEq, Clone)]
struct Tile(String, RGB);

// This is the structure that will store our game state, typically a state machine pointing to
// other structures.

#[derive(Default, Clone)]
struct State {
    map: Vec<Tile>,
}

impl State {
    pub fn new() -> State {
        // let mut default_map: Vec<Tile> = Vec::new();
        // for _ in 0..MAP_TILES {
        //     default_map.push(Tile(".".to_string(), RGB::from_f32(0.0, 0.0, 0.0)))
        // }

        let mut state = State {
            // map: default_map,
            map: vec![Tile(".".to_string(), RGB::from_f32(0.0, 0.0, 0.0)); MAP_TILES],
        };

        let mut rng = RandomNumberGenerator::new();
        let mut noise = FastNoise::seeded(rng.next_u64());
        noise.set_noise_type(NoiseType::SimplexFractal);
        noise.set_fractal_type(FractalType::Billow);
        noise.set_interp(Interp::Quintic);
        noise.set_fractal_octaves(5);
        noise.set_fractal_gain(0.6);
        noise.set_fractal_lacunarity(2.0);
        noise.set_frequency(2.0);

        for y in 0..MAP_HEIGHT {
            for x in 0..MAP_WIDTH {
                let n = noise.get_noise((x as f32) / 160.0, (y as f32) / 100.0);
                if n < 0.0 {
                    state.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, 0.0, 1.0 - (0.0 - n)));
                // ░
                } else {
                    state.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, n, 0.0));
                    // ░
                }
            }
        }
        state
    }

    // Notes on fractal settings:
    // octaves:
    // Total number of frequency octaves to generate the noise with.
    // The number of octaves control the amount of detail in the noise function. Adding more octaves increases the detail, with the drawback of increasing the calculation time.

    // frequency:
    // The number of cycles per unit length that the noise function outputs.

    // lacunarity:
    // A multiplier that determines how quickly the frequency increases for each successive octave in the noise function.
    // The frequency of each successive octave is equal to the product of the previous octave's frequency and the lacunarity value.
    // A lacunarity of 2.0 results in the frequency doubling every octave. For almost all cases, 2.0 is a good value to use.

    // persistence:
    // A multiplier that determines how quickly the amplitudes diminish for each successive octave in the noise function.
    // The amplitude of each successive octave is equal to the product of the previous octave's amplitude and the persistence value. Increasing the persistence produces "rougher" noise.

    pub fn perlin_fractal(&mut self) {
        let mut rng = RandomNumberGenerator::new();
        let mut noise = FastNoise::seeded(rng.next_u64());
        noise.set_noise_type(NoiseType::PerlinFractal);
        noise.set_fractal_type(FractalType::FBM);
        noise.set_fractal_octaves(5);
        noise.set_fractal_gain(0.6);
        noise.set_fractal_lacunarity(2.0);
        noise.set_frequency(2.0);

        for y in 0..MAP_HEIGHT {
            for x in 0..MAP_WIDTH {
                let n = noise.get_noise((x as f32) / 160.0, (y as f32) / 100.0);
                if n < 0.0 {
                    self.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, 0.0, 1.0 - (0.0 - n)));
                } else {
                    self.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, n, 0.0));
                }
            }
        }
    }

    pub fn simplex_fractal(&mut self) {
        let mut rng = RandomNumberGenerator::new();
        let mut noise = FastNoise::seeded(rng.next_u64());
        noise.set_noise_type(NoiseType::SimplexFractal);
        noise.set_fractal_type(FractalType::FBM);
        noise.set_fractal_octaves(5);
        noise.set_fractal_gain(0.6);
        noise.set_fractal_lacunarity(2.0);
        noise.set_frequency(2.0);

        for y in 0..MAP_HEIGHT {
            for x in 0..MAP_WIDTH {
                let n = noise.get_noise((x as f32) / 160.0, (y as f32) / 100.0);
                if n < 0.0 {
                    self.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, 0.0, 1.0 - (0.0 - n)));
                } else {
                    self.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, n, 0.0));
                }
            }
        }
    }

    pub fn simplex_rigid_hermite(&mut self) {
        let mut rng = RandomNumberGenerator::new();
        let mut noise = FastNoise::seeded(rng.next_u64());
        noise.set_noise_type(NoiseType::SimplexFractal);
        noise.set_fractal_type(FractalType::RigidMulti);
        noise.set_interp(Interp::Hermite);
        noise.set_fractal_octaves(10);
        noise.set_fractal_gain(0.6);
        noise.set_fractal_lacunarity(2.0);
        noise.set_frequency(2.0);

        for y in 0..MAP_HEIGHT {
            for x in 0..MAP_WIDTH {
                let n = noise.get_noise((x as f32) / 160.0, (y as f32) / 100.0);
                if n < 0.0 {
                    self.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, 0.0, 1.0 - (0.0 - n)));
                } else {
                    self.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, n, 0.0));
                }
            }
        }
    }

    pub fn simplex_billow_quantic(&mut self) {
        let mut rng = RandomNumberGenerator::new();
        let mut noise = FastNoise::seeded(rng.next_u64());
        noise.set_noise_type(NoiseType::SimplexFractal);
        noise.set_fractal_type(FractalType::Billow);
        noise.set_interp(Interp::Quintic);
        noise.set_fractal_octaves(5);
        noise.set_fractal_gain(0.6);
        noise.set_fractal_lacunarity(2.0);
        noise.set_frequency(2.0);

        for y in 0..MAP_HEIGHT {
            for x in 0..MAP_WIDTH {
                let n = noise.get_noise((x as f32) / 160.0, (y as f32) / 100.0);
                if n < 0.0 {
                    self.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, 0.0, 1.0 - (0.0 - n)));
                } else {
                    self.map[xy_idx(x as i32, y as i32)] =
                        Tile("░".to_string(), RGB::from_f32(0.0, n, 0.0));
                }
            }
        }
    }
    pub fn whitenoise(&mut self) {
        let mut rng = RandomNumberGenerator::new();
        let mut noise = FastNoise::seeded(rng.next_u64());
        noise.set_noise_type(NoiseType::WhiteNoise);
        noise.set_frequency(2.0);

        for y in 0..MAP_HEIGHT {
            for x in 0..MAP_WIDTH {
                let n = noise.get_noise(x as f32, y as f32);
                let col = (n + 1.0) * 0.5;
                self.map[xy_idx(x as i32, y as i32)] =
                    Tile("░".to_string(), RGB::from_f32(col, col, col));
            }
        }
    }
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        // New: handle keyboard inputs.
        match ctx.key {
            None => {} // Nothing happened
            Some(key) => {
                // A key is pressed or held
                match key {
                    VirtualKeyCode::Q => {
                        println!("Perlin Fractal");
                        self.perlin_fractal()
                    }
                    VirtualKeyCode::W => {
                        println!("Simplex Fractal");
                        self.simplex_fractal()
                    }
                    VirtualKeyCode::E => {
                        println!("Simplex Rigid Hermite Fractal");
                        self.simplex_rigid_hermite()
                    }
                    VirtualKeyCode::R => {
                        println!("Simplex Billow Quantic Fractal");
                        self.simplex_billow_quantic()
                    }
                    VirtualKeyCode::T => {
                        println!("White Noise");
                        self.whitenoise()
                    }
                    _ => {} // Ignore all the other possibilities
                }
            }
        }

        // We'll use batched drawing
        let mut draw_batch = DrawBatch::new();

        // Clear the screen
        draw_batch.cls();

        // Iterate the map array, incrementing coordinates as we go.
        let mut y = 0;
        let mut x = 0;
        for (_i, tile) in self.map.iter().enumerate() {
            // Render a tile depending upon the tile type; now we check visibility as well!
            // let mut glyph = "░";

            draw_batch.print_color(
                Point::new(x, y),
                tile.0.clone(),
                ColorPair::new(tile.1, RGB::from_f32(0., 0., 0.)),
            );

            // Move the coordinates
            x += 1;
            if x > MAP_WIDTH - 1 {
                x = 0;
                y += 1;
            }
        }
        // Submit the rendering
        draw_batch.submit(0).expect("Batch error");
        render_draw_buffer(ctx).expect("Render error");
    }
}

// We're storing all the tiles in one big array, so we need a way to map an X,Y coordinate to
// a tile. Each row is stored sequentially (so 0..80, 81..160, etc.). This takes an x/y and returns
// the array index.
pub fn xy_idx(x: i32, y: i32) -> usize {
    (y as usize * MAP_WIDTH) + x as usize
}

// It's a great idea to have a reverse mapping for these coordinates. This is as simple as
// index % 80 (mod 80), and index / 80
pub fn idx_xy(idx: usize) -> (i32, i32) {
    (idx as i32 % MAP_WIDTH as i32, idx as i32 / MAP_WIDTH as i32)
}

fn main() {
    let context = BTermBuilder::simple(MAP_WIDTH, MAP_HEIGHT)
        .unwrap()
        .with_title("Gene Wolfe's Noise World")
        .build()
        .unwrap();
    let gs = State::new();
    main_loop(context, gs).unwrap();
}
